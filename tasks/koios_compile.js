/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*global module, require*/

var fs = require("fs");

module.exports = function (grunt) {
    'use strict';
  
// =================================================
// IO Methods
    
    var writeFile = function (contents, path, output) {
        var defaults = {
            js: "src/koios.js",
            jsmin: "src/koios.min.js",
            scss: "src/koios.scss",
            css: "src/koios.css"
        },
            fullPath = output[path],
            parts = fullPath.split("/"),
            i = 0,
            tempPath = "";
        
        if (!output.hasOwnProperty(path)) {
            return;
        }
        
        // Make sure intermediate directories exist. 
        
        for (i = 0; i < parts.length; i += 1) {
            if (tempPath.length === 0) {
                tempPath = parts[i];
            } else {
                tempPath += "/" + parts[i];
            }
            
            if (!fs.existsSync(tempPath) && i < parts.length - 1) {
                fs.mkdirSync(tempPath);
            }
        }
        
        fs.writeFileSync(fullPath, contents);
    },
        
        readFiles = function (modulePath, files, outputFiles) {
            if (modulePath !== "") {
                if (modulePath.charAt(modulePath.length - 1) !== "/") {
                    modulePath += "/";
                }
            }
            
            var file, i, ext, fileParts;
            for (i = 0; i < files.length; i += 1) {
                file = files[i];
                fileParts = file.toLowerCase().split(".");
                ext = fileParts[fileParts.length - 1];
                if (outputFiles[ext]) {
                    outputFiles[ext].push(modulePath + file);
                }
            }
            
            return outputFiles;
        },

        readModule = function (modulePath, outputFiles) {
            var fullPath = modulePath + "/koios_module.json",
                pkg = grunt.file.readJSON(fullPath),
                files = pkg.files || [];
            
            return readFiles(modulePath, files, outputFiles);
        };
    
// =================================================
// Actual Grunt Task
    
    grunt.registerMultiTask("koios_compile", "Build KoiosJS Applications", function () {
        grunt.log.writeln("Starting Koios Compiler --------");
        
        var data = this.data,
            pkg = {},
            includeKoiosJS = true,
            pathToKoios = "KoiosJS/core",
            output = {},
            files = {
                js: ["KoiosJS/wrapper/koios_before.js"],
                scss: [],
                css: [],
                coffee: []
            },
            pkgModules = [],
            i = 0,
            userFiles = [],
            concatObj = {},
            ugObj = {};

        if (typeof data === "string") {
            pkg = grunt.file.readJSON(data);
        } else {
            pkg = data;
        }
        
        // Uncomment this line to log the contents of the package
//        grunt.log.writeln(JSON.stringify(pkg));
        
        // Determine whether or not to include KoiosJS as a whole in this build.
        if (pkg.hasOwnProperty("includeKoiosJS")) {
            includeKoiosJS = pkg.includeKoiosJS;
        }
        
        // Determine the output.
        output = (pkg.hasOwnProperty("output")) ? pkg.output : {};
        
        // Allow users to override the base KoiosJS path.
        if (pkg.hasOwnProperty("pathToKoiosJS")) {
            pathToKoios = pkg.pathToKoiosJS;
        }
        
        // Write a blank file into the js and minjs files.
        writeFile("", "js", output);
        writeFile("", "jsmin", output);
        
        // Read and include KoiosJS Core, if needed.
        if (includeKoiosJS) {
            grunt.log.writeln("1. Reading KoiosJS");
            
            files = readModule(pathToKoios, files);
        } else {
            grunt.log.writeln("1. (Ignoring KoiosJS)");
        }
        
        // Read and include any modules in the package definition
        grunt.log.writeln("2. Reading Koios Modules");
        pkgModules = (pkg.hasOwnProperty("modules")) ? pkg.modules : [];
        
        for (i = 0; i < pkgModules.length; i += 1) {
            files = readModule(pkgModules[i], files);
        }
        
        // Read any additional files included by the user
        grunt.log.writeln("3. Reading User Files");
        userFiles = (pkg.hasOwnProperty("files")) ? pkg.files : [];
        
        for (i = 0; i < userFiles.length; i += 1) {
            files = readFiles("", userFiles, files);
        }
        
        // End the self executing anonymous function wrapper.
        files.js.push("KoiosJS/wrapper/koios_after.js");

        // TODO: Compile The CoffeeScript
        grunt.log.writeln("4. Skipping CoffeeScript");
        
        // Concat the whole JS shebang together
        if (output.hasOwnProperty("js")) {
            grunt.log.writeln("5. Writing Concatenated, Unminified JavaScript");
            concatObj.src = files.js;
            concatObj.dest = output.js;
            
            grunt.config("concat.koios", concatObj);
            grunt.task.run("concat:koios");
            
            if (output.hasOwnProperty("jsmin")) {
                grunt.log.writeln("6. Minifying JavaScript");
                ugObj[output.jsmin] = output.js;
                
                grunt.config("uglify.koios", {files: ugObj});
                grunt.task.run("uglify:koios");
            } else {
                grunt.log.writeln("6. (Skipping Minification)");
            }
            
        } else {
            grunt.log.writeln("5. (Skipping JavaScript)");
            grunt.log.writeln("6. (Skipping Minification)");
        }
        
        // Compile & Concatenate SCSS/CSS
        if (output.hasOwnProperty("css") && output.hasOwnProperty("scss")) {
            if (files.scss.length > 0) {
                grunt.log.writeln("7. Concatenating & Compiling SASS/SCSS");
                
                concatObj = {};
                concatObj.src = files.scss;
                concatObj.dest = output.scss;
                
                grunt.config("concat.koios_scss", concatObj);
                grunt.task.run("concat:koios_scss");
                
                ugObj = {files: {}};
                ugObj.files[output.css] = output.scss;
                
                grunt.config("sass.koios_scss", ugObj);
                grunt.task.run("sass:koios_scss");
                
                files.css.push(output.css);
            } else {
                grunt.log.writeln("7. (Ignoring SASS, no files to compile)");
            }
            
            grunt.log.writeln("7. Concatenating CSS");
            concatObj = {};
            concatObj.src = files.css;
            concatObj.dest = output.css;
            
            grunt.config("concat.koios_css", concatObj);
            grunt.task.run("concat:koios_css");
        } else {
            grunt.log.writeln("7. (Ignoring SASS)");
            grunt.log.writeln("8. (Ignoring CSS)");
        }
        
        grunt.log.writeln("Koios Compile Complete.");
        
        // Uncomment this line to log the contents of the files we'll be reading
        grunt.log.writeln(JSON.stringify(files));
    });
    
};
